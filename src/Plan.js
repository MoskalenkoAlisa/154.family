import React, { PureComponent } from 'react';
import jsonData from './data/p1.json';
import * as d3 from "d3";
import { pedigreeTree } from '@solgenomics/d3-pedigree-tree';
//import pedigreeTree from './d3-pedigree-tree/src/d3-pedigree-tree';

import { withRouter } from 'react-router-dom';
import queryString from 'query-string';

// eslint-disable-next-line
String.prototype.replaceAt = function(index, replacement){
  return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}


class Plan extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      onlyFor:null,
      items: [],
      original: [],
      expaneded:[]
    };
  }

  leaveOnlyMainPathLevel(items, node, way = 0)
  {
    if (!node) return;
    node.way = way;
    node.kill = false;
     //console.log('не удаляем', node.text);
     
     if (this.state.expaneded.includes(node.id)) 
     {
       console.log('in expa !', this.state.expaneded);
       console.log('in expa 3', items);
     }
    // eslint-disable-next-line 
    if (node.mother && node.mother.kill === true && (node.way%19===0 || node.way%13===0) || this.state.expaneded.includes(node.id) ) this.leaveOnlyMainPathLevel(items, node.mother, way+13);
    // eslint-disable-next-line
    if (node.father && node.father.kill === true && (node.way%19===0 || node.way%13===0) || this.state.expaneded.includes(node.id) ) this.leaveOnlyMainPathLevel(items, node.father, way+13);

    items.filter(c=> c.father && c.father.id === node.id && c.kill === true && (node.way%19===0 || node.way%13===0 || this.state.expaneded.includes(node.id) ) ).map(c=> 
      { console.log('?',c);
        return this.leaveOnlyMainPathLevel(items, c, way+19);
      });
     items.filter(c=> c.mother && c.mother.id === node.id && c.kill === true && (node.way%19===0 || node.way%13===0 || this.state.expaneded.includes(node.id)  ))
     .map(c=> this.leaveOnlyMainPathLevel(items, c, way+19 ));
  }

  leaveOnlyMainPath(onlyfor)
  {
    let tempItems = [...this.state.items]
    tempItems.map(c=>c.kill = true);
    // eslint-disable-next-line
    let node = tempItems.filter(c=>c.id == onlyfor)[0];
    if (node) this.leaveOnlyMainPathLevel(tempItems, node, 0);
    else console.log('Node '+onlyfor +" not found");
    // set + to hidden parent
    tempItems.filter(c=> (c.father && c.father.kill === true) || (c.mother && c.mother.kill === true)).forEach(c=>{ c.expandableParent = true; 
      console.log('c', c);
      c.mother=null;
      c.father=null;
    }); 

    // set + to hidden child 
    tempItems.filter(c=> c.mother&&c.mother.kill === false && c.kill === true).forEach(c=>{ c.mother.expandableChild = true});
    tempItems.filter(c=> c.father&&c.father.kill === false && c.kill === true).forEach(c=>{ c.father.expandableChild = true});

    // clean from related
    tempItems = tempItems.filter(c=>c.kill === false);
    return tempItems;
  }

  update()
  {
    if (this.state.onlyFor)
    {// for person id 
      console.log('only for id: ', this.state.onlyFor);
      this.setState({ items: this.leaveOnlyMainPath(this.state.onlyFor)}, ()=> { this.draw(this.state.items); });
    }
    else this.draw(this.state.items); //full tree
  }

  componentDidMount = () => {
    let original = JSON.parse(JSON.stringify(jsonData));
    original.forEach(d => {
      d.mother = original.filter(c => c.id === d.mother)[0];
      d.father = original.filter(c => c.id === d.father)[0];
    });

     
    const value = queryString.parse(this.props.location.search);
    this.setState({items: original, original:original, onlyFor: value.id}, ()=> this.update());
  }

  toggleExpaneded(node)
  {
    if (this.state.expaneded.includes(node.id))
    {// и

    }
    else
    {
      var joined = this.state.expaneded.concat(node.id);
      console.log('joi', joined);
      this.setState({expaneded: joined, items: this.state.original}, ()=>this.update());
    }
  }

  draw(nodes2) {
    var tree = pedigreeTree()
      .levelWidth(300)
      .nodePadding(100)
      .nodeWidth(70)
      .linkPadding(125)
      .parents(function (d) {
        return [d.mother, d.father].filter(Boolean);
      })
      .groupChildless(false)
      .iterations(10)
      .data(nodes2);

    this.drawTree(tree(), ".canv");
    /* for what reason?
    var s = 999
    var end = false;
    var ended = false;
    var that = this;
     
    // eslint-disable-next-line
    var inter = setInterval(function () {
     if (s < 1000 && !(end && ended)) that.drawTree(tree.iterations(s++)(), ".canv", d3.transition().duration(400).ease(d3.easeLinear));
      if (end) ended = true;
    }, 500);
    */
    function clicky() {
      // end = true;
      // ended = false;

      // eslint-disable-next-line
      d3.select("body").on("click", null).on("click", clicky = () => {
        d3.select("body").on("click", clicky());
        // s = 0
        // end = false;
      });
    }
    d3.select("body").on("click", clicky());

  }

  capitalise(text)
  {
    return text ? text.split(' ').map(s=> (s.length>1 ? s.replaceAt(0, s[0].toUpperCase()) : s)  ).join(' ') : '?';
  }
/*
  toggleVisibility(node)
  {
    console.log('6', node);
    let ids = [...this.state.items]
    let index = ids.findIndex(d=>d.id === node.id);
    let d = ids[index]; 

    //console.log('7', d);
    
    if (d.father) {
        d._father = d.father;
        d.father = null;
        d._mother = d.mother;
        d.mother = null;
        
      } else {
        d.father = d._father;
        d._father = null;
        d.monther = d._monther;
        d._monther = null;
     }

     ids[index] = d;
     // 
     console.log('ids', ids);
     this.setState({ items: ids }); 
     this.update();
  }
*/
  drawTree(layout, svg_selector, trans) {

    //set default change-transtion to no duration
    trans = trans || d3.transition().duration(100);
    var that = this;
    //make wrapper(pdg)
  //  console.log(layout);
    var canv = d3.select(svg_selector);
  //  console.log(canv.node());
    var cbbox = canv.node().getBoundingClientRect();
    var canvw = cbbox.width,
      canvh = cbbox.height;
    var pdg = canv.select('.pedigreeTree');
    if (pdg.empty()) {
        pdg = canv.append('g').classed('pedigreeTree', true);
    }

    //make background
    var bg = pdg.select('.pdg-bg');
    if (bg.empty()) {
      bg = pdg.append('rect')
        .classed('pdg-bg', true)
        .attr('width', canvw)
        .attr('height', canvh)
        .attr('fill', "white")
        .attr('stroke', 'none');
    }


    //make scaled content/zoom groups
    var padding = (layout.x[1])/15;//150;
    var pdgtree_width = layout.x[1];
    var pdgtree_height = layout.y[1];
   // var scale = this.get_fit_scale(canvw, canvh, pdgtree_width, pdgtree_height, padding);

    var scale  = (canvw) / (pdgtree_width + padding*3);
    var offsetx = (canvw - pdgtree_width * scale) / 2;
    var offsety = (canvh - pdgtree_height * scale) / 2;
    if (offsetx<=0) offsetx = padding;
    if (offsety<=0) offsety = padding;


    var content = pdg.select('.pdg-content');
    if (content.empty()) {
      var zoom = d3.zoom();
      var zoom_group = pdg.append('g').classed('pdg-zoom', true).data([zoom]);

      content = zoom_group.append('g').classed('pdg-content', true);

      zoom.on("zoom", function () {
        // console.log('zoom');
        zoom_group.attr('transform', d3.event.transform);
      }).scaleExtent([0.2, 10 / scale]);
      bg.style("cursor", "all-scroll").call(zoom).call(zoom.transform, d3.zoomIdentity);
      bg.on("dblclick.zoom", function () {
        zoom.transform(bg.transition(), d3.zoomIdentity);
        return false;
      });

/*
      content.attr('transform',
        d3.zoomIdentity
          .translate(offsetx, offsety)
          .scale(scale)
      );
  */   
    }

    content.transition(trans)
      .attr('transform',
        d3.zoomIdentity
          .translate(offsetx, offsety*scale)
          .scale(scale)
      );
      // console.log('scale=',scale);
      // console.log('offsetx=',offsetx);
      // console.log('offsety=',offsety);

    //set up draw layers
    var linkLayer = content.select('.link-layer');
    if (linkLayer.empty()) {
      linkLayer = content.append('g').classed('link-layer', true);
    }
    var nodeLayer = content.select('.node-layer');
    if (nodeLayer.empty()) {
      nodeLayer = content.append('g').classed('node-layer', true);
    }

    //draw nodes
    var nodes = nodeLayer.selectAll('.node')
      .data(layout.nodes, function (d) { return d.id; });

    var newNodes = nodes.enter().append('g')
      .classed('node', true)
      .attr('transform', function (d) {
        var begin = d;
        if (d3.event && d3.event.type === "click") {
          begin = d3.select(d3.event.target).datum();
        }
        return 'translate(' + begin.x + ',' + begin.y + ')'
      });

    newNodes.append('rect')
      .attr("fill", function (c) {
        // if (c.value.mandatory == false) return d3.rgb(150,168,150);

        return d3.rgb(135, 168, 222);
      })//isfinished 
      .attr("width", 150)
      .attr("height", 20)
      .attr("y", -10);
/*
    newNodes.each(function (d, i, arr) {

      if (d.value.isfinished === false) {
        d3.select(this).selectAll('recta')
          .data(d.modules)
          .enter()
          .insert('rect')
          .attr("height", function (c) { return (c.Weight) * 3 + 3; })
          .attr("width", 5)
          .attr("x", function (d, i) { return (i) * 7 + 15 })
          .attr("y", 12)
          .attr("fill", function (d, i) { return d.Mandatory === true ? d3.rgb(135, 168, 222) : d3.rgb(221, 227, 200) })
          .on("click", function (d) {
            console.log('9');
            if (d.Mandatory === false) {
              if (d.Selected === true) {
                  d.Selected = false;
                d3.select(this).style("fill", d3.rgb(221, 227, 200));

              }
              else {
                d.Selected = true;
                d3.select(this).style("fill", "green");
              }
            }
          });
      }

    });

  */ 
    newNodes.append('circle')
      .attr('r', 10)
      .attr('fill', function (d) {
        return d.type === 'node' ? 'black' : 'grey';
      })
      .on("click", function (node) {
          console.log('click 55', node);
        
        //  d.attr("display", "none");
        //layout.pdgtree.excludeFromGrouping[d.id]=true;
        
       //that.toggleVisibility(d);
       //that.hideHeirs(node);
      });

  


    newNodes.append('text')
      .attr('x', 10)
      .attr('y', -15) 
      .text(function (d) {  return d.value.text } );

    var allNodes = newNodes.merge(nodes);
    allNodes.transition(trans).attr('transform', function (d) {
      return 'translate(' + d.x + ',' + d.y + ')'
    })
    newNodes.append("text")
    .attr("dx",-23)
    .attr("dy",".35em")
    .text(function(d){ return d.value.expandableParent ? '+' : null  } )
    .on("click", function (node) {
      console.log('click 77', node);
      that.toggleExpaneded(node); })
    
      newNodes.append("text")
    .attr("dx", 150+3)
    .attr("dy",".35em")
    .text(function(d){ return d.value.expandableChild ? '+' : null  } )
    .on("click", function (node) {
      console.log('click 77', node);
      that.toggleExpaneded(node); });

    

    allNodes.select('text').text(function (d) { return that.capitalise(d.value.text); });
    allNodes.filter(function (d) { return d.type === "node-group" })
      .style("cursor", "pointer")
      .on("click", function (d) {
        console.log('click 6');
       // layout.pdgtree.excludeFromGrouping[d.id]=true;
        this.drawTree(layout.pdgtree(), ".canv", d3.transition().duration(700).ease(d3.easeLinear));
      })
      .select("circle")
      .attr('fill', "white")
      .attr('stroke', "black");
    //var oldNodes = nodes.exit().remove();

    //link curve generators
    var stepline = d3.line().curve(d3.curveStep);
    var curveline = d3.line().curve(d3.curveBasis);
    var build_curve = function (d) {
      if (d.type === "parent->mid") return curveline(d.path);
      if (d.type === "mid->child") return stepline(d.path);
    };

    //link colors
    var link_color = function (d) {
      if (d.type === "mid->child") return 'green';
      if (d.type === "parent->mid") {
        //if its the first parent, red. Otherwise, blue.

        // if (d.source && d.source.value) console.log(d.source);

        if (d.source && d.source.value.fixed === true) return 'grey'
        return d.sinks[0].parents.indexOf(d.source) ? 'blue' : 'green';
      }
      return 'gray';
    }

    var link_width = function (d) {

      if (d.source && d.source.value.fixed === true) return 7;
      return 2;
    }

    //make links
    var links = linkLayer.selectAll('.link')
      .data(layout.links, function (d) { return d.id; });
    var newLinks = links.enter().append('g')
      .classed('link', true);
    newLinks.append('path')
      .attr('d', function (d) {
        var begin = (d.sink || d.source);
        if (d3.event && d3.event.type === "click") {
          begin = d3.select(d3.event.target).datum();
        }
        return curveline([[begin.x, begin.y], [begin.x, begin.y], [begin.x, begin.y], [begin.x, begin.y]]);
      })
      .attr('fill', 'none')
      .attr('stroke', link_color)
      .attr('stroke-width', link_width);
    var allLinks = newLinks.merge(links);
    allLinks.transition(trans).select('path').attr('d', build_curve);
    //oldNodes = links.exit().remove();
  }


  get_fit_scale(w1, h1, w2, h2, pad) {
    w1 -= pad * 2;
    h1 -= pad * 2;
    if (w1 / w2 < h1 / h2) {
      return w1 / w2;
    } else {
      return h1 / h2;
    }
  }


  render() {
    return (<div>
              <svg className='canv' style={{ border: 'none thin black', width: '98%', height: '97vh' }} ></svg>
           </div>
           );
  }

}


//export default Plan;
export default withRouter(Plan);